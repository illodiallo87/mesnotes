# Bootstrap


## Introduction


* bootstrap est un framework open source de developpement web orienté interface graphique.


## Partie:1



 ###  installation de bootstrap
 

 
###  Étapes
 
1.Créer un nouveau projet sur Webstorm
 
2.Créer les dossiers (assets, main, xp)

3.Ouvrir le terminal et saisir `npm init`
 
4.Puis mettre entrer à chaque fois jusqu'au package name

5.Entrer le nom (my-cv par ex)

6.Saisir npm install bootstrap

7.Une fois le dossier node module créée, créer le fichier html dans main
 
8.Relier le fichier bootstrap
 
 `<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.css">`
 
 9.Créer un container :
 
 `<div class="container"> </div>`
 
 10.Créer à l'interieur du container une grille :
 
 ````
 <div class="row">
        <div class="col numbers">1</div>
        <div class="col numbers">2</div>
        <div class="col numbers">3</div>
    </div>

    <div class="row">
        <div class="col numbers">1</div>
        <div class="col numbers">2</div>
        <div class="col numbers">3</div>
        <div class="col numbers">4</div>
    </div>
````

11.Créer un fichier CSS
   
   
12.Relier le CSS au fichier HTML avec le code suivant :

`<link rel="stylesheet" href="../assets/styles.css">`




## Partie:2 





###  difference entre container classique et container-fluid




* `Container classique` va ce centrer sur la page.


```
<div class="container">
    <div class="row">
        <div class="col"></div>
    </div>
</div>
```





* Alors que `container-fluid` prend toute la page.

```
<div class="container-fluid">
    <div class="row">
        <div class="col"></div>
    </div>
</div>

```





## Partie:3
 



###   signification row et col



* `row` = ligne

* `col` = colonne

```

<div class="container">
    <div class="row">
        <div class="col"></div>
    </div>
</div>

```







``important d'avoir toujours un col et un row``



###  Taille des col


*  Il ya 12 colonnes au total. Si on veut deux colonnes dont la première fait un tiers de la page et l'autre deux tiers 


```

<div class="col-4></div>
<div class="col-8></div>

```




* Pour décaler à gauche (dans les colonnes non utilisées) on met ` offset-1` :




```
<div class="col-7 offset-1></div>
```

* Si on met un background, il faut mettre un contenu pour voir la couleur.








``toujours finir un row par un col ( row,col,row,col) mais pas l'inverse``





* jamais modifier le contenu css bootstrap.
