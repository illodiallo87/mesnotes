# Les objets



### Déstructuration des objets



Il s'agit de manipuler les objets plus facilement.




### Ancienne méthode



````

const user = {
    firstName: 'Seb',
    lastName: 'LeProf',
    age: 34,
    billionaire: false,
    eyes: 'blue'
};

const age = user.age;
const lastName = user.lastName;

console.log(age, lastName);


````

La console va renvoyer l'âge et le nom.




### Nouvelle méthode



````

const user = {
    firstName: 'Seb',
    lastName: 'LeProf',
    age: 34,
    billionaire: false,
    eyes: 'blue'
};

const {age, lastName} = user;

console.log(age, lastName);

````

### Rajouter des attributs à un objet


````

const user = {
    firstName: 'Seb',
    lastName: 'LeProf',
    age: 34,
    billionaire: false,
    eyes: 'blue'
};

`user.adress = '2 rue des fleurs` 


````


On peut aussi rajouter l'adresse directement dans l'objet à la suite.




### Avoir accès à un attribut inconnu



C'est une notation qui permet d'avoir accès à l'attribut d'un objet quand on ne connaît pas le nom de cet attribut à l'avance.



````

const attrs = ['lastName', 'age'];

attrs.forEach(attr => console.log(user[attr]))

````

const attrs = ['lastName', 'age'];

attrs.forEach(attr => console.log(user[attr]))

