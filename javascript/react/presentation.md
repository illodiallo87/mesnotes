# SOMMAIRE:

*  [Introduction REACT](Introduction React)

*  [Racine de l'application](Racine de l'application)

*  [Creer un element](Creer un element)

*  [Utiliser JSX pour faire du react](Utiliser JSX pour faire du react)

*  [Interpolation](Interpolation)

*  [Les composants](Les composants)

*  [Fonction composants et composants à base de classes](Fonction composants et composants a base de classe) 

*  [Produire le rendu d'un composant](Produire le rendu d'un composant)

*  [Composition de composants](Composition de composants)

*  [Les props (proprietes)](Les props(proprietes))

*  [Décomposition des props](Decomposition des props)

*  [Element enfant en JSX](Element enfant en JSX)

*  [Les fragments](Les fragments)

*  [Créer un composant](Créer un composant)

*  [Passer les bons types](Passer les bons types)

*  [Spécifier les types des props](Specifier les types des props)

*  [Mettre du CSS](Mettre du CSS)

*  [Passer des classes CSS dans un component](Passer des classes CSS dans un component)

*  [Choisir une classe grâce à une props](Choisir une classe grâce a une props)

*  [La prop style](La prop style)

*  [Gérer les événements](Gerer les evenements)

*  [Gérer les clics](Gerer les clics)

*  [Gérer tous types d'évenements](Gerer tous types d'evenements)

*  [Gérer les états proprement](Gerer les etats proprement)

*  [Structure d'un formulaire](Structure d'un formulaire)

*  [L'affichage conditionnel](L'affichage conditionnel)

*  [Rendre une liste](Rendre une liste)

*  [Passer des functions en props](Passer des functions en props)

*  [Gérer le monde extérieur](Gerer le monde exterieur)

*  [React useEffect](React useEffect)












# Introduction React


React.Js, est une techno Javascript, mais il ne s’agit pas d’un framework à proprement parler. En fait, il s’agit plus d’une librairie open source qui permet de construire des interfaces utilisateur dynamique à l'aide du virtual DOM.


###  Racine de l'application



Dans notre fichier HTML, on va ajouter une balise « div » avec un identifiant (id). Par convention on l'appelera « root », ce sera la racine de notre application :
`<div id="root"></div>`



### Créer un élément




````

// RECUPERER LE DOM D'UN ELEMENT PAR SON ID

<div id="root"></div>
const root = document.getElementById('root');

// CREER UN ELEMENT (DIV) 

const exampleDiv = React.createElement('div', {
    className: 'main',
    children: `Bienvenue dans l'initiation à ${value} (version ${version})`,
  });
  ReactDOM.render(exampleDiv, root);

````



* children : le contenu de cette DIV qui est créer à l'interieur de root


*  reactDom.render : on prend le Dom virtuel et on affiche (render) exampleDiv et root.


###  Utiliser JSX pour faire du react


Pour faciliter l'utilisation de react et melanger le html et js on utilisera JSX.
Pour se faire on utilisera BABEL (un compileur), babel va transformer le JSX en javascript.




````

// CREER LE MEME ELEMENT EN JSX
const exampleDiv = <div className="main">
    Bienvenue dans l'initiation à ${value} (version ${version})
  </div>;

````


### Interpolation


L'interpolation est un calcul fait à l'interieur d'un template et qui se trouve entre des crochets.
Un template est une div (exampleDiv).


### Les props (propriétés)



Ce sont les propriétés que l'on donne aux composants.
La variables JSX qui contient les propriétés va renvoyer une div (ou un autre élément)
et si la div est un children ou un className elle va les mettre à l'interieur du contenu
sinon elle va créer des attributs et leur donner le nom (type) et la valeur(button).

````

<script type="text/babel">
  const root = document.getElementById('root');

  const className = 'main';
  const children = 'Coucou de White Rabbit';

  /*const exampleDiv = React.createElement('div', {className, children,});*/

  const props = {children, className};
  const exampleDiv = <div {...props} />;

  ReactDOM.render(exampleDiv, root);
</script>

````


### Les fragments


````

<script type="text/babel">
  const root = document.getElementById('root');

  const className1 = 'main';
  const children1 = 'ma div1';

  const className2 = 'secondary';
  const children2 = 'ma div2';

  const props1 = {
    className: className1,
    children: children1,
  };

  const props2 = {
    className: className2,
    children: children2,
  };
  const exampleDiv = <div>
    <div {...props1} />
    <div {...props2} />
  </div>;

  ReactDOM.render(exampleDiv, root);
</script>

````



Pour afficher les deux props il faut mettre les deux div séparées à l'interieur de la div principal (voir ci-dessus)
Le problème c'est que les deux div sont à l'inteireur d'une div inutile, pour remedier à ce probleme on utilise les fragments (on met des balises vides) :


````

<script type="text/babel">
  const root = document.getElementById('root');

  const className1 = 'main';
  const children1 = 'ma div1';

  const className2 = 'secondary';
  const children2 = 'ma div2';

  const props1 = {
    className: className1,
    children: children1,
  };

  const props2 = {
    className: className2,
    children: children2,
  };
  const exampleDiv = <>
    <div {...props1} />
    <div {...props2} />
  </>;

  ReactDOM.render(exampleDiv, root);
</script>

````

### Créer un composant



````

function getTitle(className, title, number) {
    return <div className={className}>{`Titre n°${number}: ${title}`}</div>
  }

const exampleDiv =
      <div>
        {getTitle('main', 'coucou', 1)}
      </div>

````

Se composant est lourd, on va donc le faire d'une autre manière :



````

<script type="text/babel">
  const root = document.getElementById('root');

  function getTitle(className, title, number) {
    return <div className={className}>{`Titre n°${number}: ${title}`}</div>
  }

  function MyTitle({className, children, number}) {
    const props = {
      children: `Titre n°${number}: ${children}`,
      className
    };
    return <div {...props} />

  }

  const exampleDiv =
      <div>
        {getTitle('main', 'coucou', 1)}
        <MyTitle className="main" number="1">coucou</MyTitle>
      </div>

  ReactDOM.render(exampleDiv, root);
</script>
<script type="text/babel">
  const root = document.getElementById('root');

  function Hello({firstName, lastName, age, car}) {
    return (
        <div>
          <div>age : {typeof age}</div>
          <div>car : {typeof car}</div>
          Bonjour je m'appelle {firstName} {lastName} et j'ai {age} an(s)!
        </div>
    );
  }

  const exampleDiv = <Hello
      firstName='sebastien'
      lastName='Bianchi'
      age={17}
      car={true}
  />;

  ReactDOM.render(exampleDiv, root);
</script>
````


### Spécifier les types des props


On va ajouter la librairie et ensuite on va spécifier les types avec .propTypes




````

<script src="https://unpkg.com/prop-types@15.6/prop-types.js"></script>

<script type="text/babel">
  const root = document.getElementById('root');

  function Hello({firstName, lastName, age, car}) {
    return (
        <div>
          <div>age : {typeof age}</div>
          <div>car : {typeof car}</div>
          Bonjour je m'appelle {firstName} {lastName} et j'ai {age} an(s)!
        </div>
    );
  }

  Hello.propTypes = {
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    age: PropTypes.number.isRequired,
    car: PropTypes.bool,
  };

  const exampleDiv = <Hello
      lastName='Bianchi'
      age={17}
      car={true}
  />;

  ReactDOM.render(exampleDiv, root);
</script>

````



Si jamais on se trompe et on met un string au lieu d'un boolean,
il y aura une erreur qui va s'afficher dans la console.
Si on ne met pas de car du tout il ne va rien se passer il y aura simplement le car en moins dans le component.




### Mettre du CSS


````

<style>
    .container {
        border: 2px solid black;
    }
</style>

<script type="text/babel">
  const root = document.getElementById('root');

  function Box({...props}) {
    return (
        <div
          className="container bg-blue"
          {...props}
        />
    )
  }

  const exampleDiv = (
      <>
        <Box>
        mon container
        </Box>
      </>

  ReactDOM.render(exampleDiv, root);
</script>

````


### Passer des classes CSS dans un component



````
<style>
    .container {
        border: 2px solid black;
    }

    .bg-blue {
        background-color: #1edcff;
    }
    .bg-red {
        background-color: #ff5454;
    }
    .big {
        font-size: 50px;
    }
</style>

<script type="text/babel">
  const root = document.getElementById('root');

  function Box({className, ...props}) {
    return (
        <div
          className={`container ${className}`}
          {...props}
        />
    )
  }

  const exampleDiv = (
      <>
        <Box className="bg-blue big>
        mon container
        </Box>

        <Box className="bg-red>
        mon container
        </Box>
      </>

  ReactDOM.render(exampleDiv, root);
</script>

````


On a ajouté des couleurs et une taille.



### Choisir une classe grâce à une props



````

<style>
    .container {
        border: 2px solid black;
    }

    .bg-blue {
        background-color: #1edcff;
    }
    .bg-red {
        background-color: #ff5454;
    }
    .small {
        width: 50px;
        height: 50px;
    }
    .medium {
        width: 100px;
        height: 100px;
    }
    .large {
        width: 150px;
        height: 150px;
    }
</style>

<script type="text/babel">
  const root = document.getElementById('root');

  function Box({className, size = '', ...props}) {
    let sizeClass;
    if (size === 'big') {
      sizeClass = 'large'
    }
    if (size === 'middle') {
      sizeClass = 'medium'
    }
    if (size === 'little') {
      sizeClass = 'small'
    }
    return (
        <div
            className={`container ${className} ${sizeClass}`}
            {...props}
        />
    )
  }

  const exampleDiv = (
      <>
        <Box size="big" className="bg-blue">
        mon container
        </Box>
        <Box size="little" className="bg-red">
        mon container
        </Box>
      </>
  );

  ReactDOM.render(exampleDiv, root);
</script>

````

