#   Flexbox


##    Introduction





*  Il faut un ```container```principal dans lequel on pourra mettre les blocs qu'on veut avec flexbox.




##   Partie:1




###   Dans le container




*   Dans ce container on utilisera les propriétés de flexbox.




*    Il faut mettre un width et un height pour que les blocs apparaissent.




*    Si on met un container dans un element d'un autre container ça crée les blocs a l'interieur d'un bloc.









````
.container {
    display: flex; (les blocs se mettent en ligne)
    justify-content: center; (les blocs se mettent au centre)
    justify-content: space-between; (Pour mettre des espaces entre les blocs)
    flex-wrap: wrap;
}   
````




  














