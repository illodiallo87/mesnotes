# Créer des liens

## Introduction

* Pour créer des liens entre deux pages on utilise la balise ```a```

````
<a href="../main/index.html">
````

* Dans l'exemple ci-dessus le lien renvoie vers une page appelé index.html