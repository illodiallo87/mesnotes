# Raccourcis clavier

*  `Ctrl+C `: copier

* `Ctrl+X` : couper.

*  `Ctrl+V` : coller.

* `Ctrl+Z` : annuler.


* `Ctrl+Y` : rétablir.

* `Ctrl+A` : tout sélectionner.

* `Ctrl+P` : imprimer.

* `F1` : afficher l’aide.

* `Ctrl+Alt+Suppr` : pour ouvrir le gestionnaire de tâche ou verrouiller l’ordinateur.

* `Windows ou Ctrl+Echap` : ouvrir le menu démarrer/basculer sur le bureau windows8.

* `AltGr+6` : pour faire un pipe |

* `AltGr+7`  : pour faire les bactiks


*  `ctrl + A` : sélectionner tout le contenu

*  `ctrl + F` : faire une recherche

*  `ctrl + R `: rafraîchir la page

*  `ctrl + tab` : basculer sur l'onglet suivant

*  `ctrl + shift + tab` : revenir sur l'onglet précédent

*  `shift + flèche `: sélectionner le texte

*   `shift + ctrl + flèche` : sélectionner un mot

*   `ctrl + z` : revenir en arrière

*   `shift + ctrl + z `: annuler le retour en arrière

*   `ctrl + d` : dupliquer

*   `alt + j` : sélectionner une occurrence

*   `ctrl + y `: supprimer une ligne

*   `ctrl + x `: couper une ligne

*   `alt + ctrl + l` : remettre en forme

*   `ctrl + espace` : voir les choix possibles

*   `ctrl + s` : enregistrer

*   `shift + ctrl + flèche haut ou bas` :déplacer la ligne

*   `shift + f6 `: changer le nom partout

*   `ctrl + -` : réduire la ligne
